import Vue from 'vue'
// import store from '@/store'
import store from './store/index'
import router from '@/router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import axios from 'axios'
import App from '@/App.vue'
import './registerServiceWorker'

Vue.use(Vuetify)


axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'


Vue.config.productionTip = false


new Vue({
  router,
  store,

  render: h => h(App)
}).$mount('#app')
