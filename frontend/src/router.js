import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/pages/Home.vue'
import Post from '@/pages/Post.vue'
import Login from '@/pages/Login.vue'
import Register from '@/pages/Register.vue'
import DashboardMain from '@/pages/DashboardMain.vue'
import EditPost from '@/pages/EditPost.vue'
import CreatePost from '@/pages/CreatePost.vue'
import ModeratePosts from '@/pages/DashboardModerate'
import ModeratePost from '@/pages/DashboardModeratePost'

const routes = [
  {
    path: '*',
    name: 'home',
    component: Home
  },
  {
    path: '/post/:id',
    name: 'post',
    component: Post,
    props: true
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DashboardMain,
  },
  {
    path: '/edit/post/:id',
    component: EditPost
  },
  {
    path: '/new/post',
    component: CreatePost
  },
  {
    path: '/moderate',
    name: 'moderate',
    component: ModeratePosts
  },
  {
    path: '/moderate/post/:id',
    component: ModeratePost
  },
]

Vue.use(VueRouter)
const router = new VueRouter({
  scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0}
  },
  mode: 'history',
  routes
})

export default router
