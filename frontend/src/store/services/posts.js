import axios from 'axios'

const state = {
  posts: [],
}

const getters = {}

const mutations = {
  setPosts(state, posts) {
    state.posts = posts
  },
  setPost(state, post) {
    state.post = post
  },
}

const actions = {
  getPostsList(context) {
    return axios.get('/api/posts')
      .then(response => {
        context.commit('setPosts', response.data)
      })
      .catch(e => {
        console.log(e)
      })
  },
  getPost(context, postID) {
    return axios.get('/api/posts/' + postID)
      .then(response => {
        context.commit('setPost', response.data)
      })
      .catch(e => {
        console.log(e)
      })
  },
  createPost(context, payload) {

    return axios.post('/api/posts/', payload)
      .then(response => {
      })
      .catch(e => {
        console.log(e)
      })
  },
  editPost(context, payload) {
    return axios.put('/api/posts/' + payload.id, payload)
      .then(response => {
      })
      .catch(e => {
        console.log(e)
      })
  },
  approvePost(context, payload) {
    return axios.put('/api/posts/approve/', payload)
      .then(response => {
      })
      .catch(e => {
        console.log(e)
      })
  },
  deletePost(context, postID) {
    return axios.delete('/api/posts/' + postID)
      .then(response => {
      })
      .catch(e => {
        console.log(e)
      })
  },
}

export default {
  state,
  getters,
  mutations,
  actions
}
