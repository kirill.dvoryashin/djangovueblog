import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import users from './services/users'
import posts from './services/posts'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    users,
    auth,
    posts,
  }
})

export default store
