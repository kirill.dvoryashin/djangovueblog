import axios from 'axios'

const state = {
  loggedIn: false,
  loggedInStorage: JSON.parse(localStorage.getItem('loggedInStorage') || "{}"),
  profileStorage: JSON.parse(localStorage.getItem('profileStorage') || null),
  profile: {},
  validation: {email: true},
  authError: false
}

const getters = {}

const mutations = {
  login(state) {
    state.loggedIn = true
    localStorage.setItem("loggedInStorage", JSON.stringify(state.loggedInStorage))
  },
  logout(state) {
    state.loggedIn = false
    localStorage.removeItem("loggedInStorage");
    localStorage.removeItem("profileStorage");
  },
  setProfile(state, payload) {
    state.profile = payload
    localStorage.setItem("profileStorage", JSON.stringify(payload))
  },
  setValidationEmail(state, bool) {
    state.validation.email = bool
  },
  setAuthError(state, bool) {
    state.authError = bool
  }
}

const actions = {
  postLogin(context, payload) {
    return axios.post('/api/users/login/', payload)
      .then(response => {
        context.commit('login')
        context.commit('setProfile', response.data)
      })
      .catch(e => {
        context.commit('setAuthError', true)
        console.log(e)
      })
  },
  postRegister(context, payload) {
    return axios.post('/api/users/register/', payload)
      .then(response => {
        if (response.data.status === 210) {
          context.commit('setValidationEmail', false)
        } else {
          context.commit('setValidationEmail', true)
          context.commit('login')
          context.commit('setProfile', response.data)
        }
      })
      .catch(e => {
        console.log(e)
      })
  },
  getProfile(context) {
    return axios.get('/api/users/profile')
      .then(response => {
        context.commit('login')
        context.commit('setProfile', response.data)
      })
      .catch(e => {
        context.commit('logout')
        console.log(e)
      })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
