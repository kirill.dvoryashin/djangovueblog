from rest_framework import routers
from apps.api.views import UserViewSet, PostViewSet

# Settings
api = routers.DefaultRouter()
api.trailing_slash = '/?'

# Users API
api.register(r'users', UserViewSet, 'users')
api.register(r'posts', PostViewSet, 'posts')
