import pytest
from django.urls import reverse
from rest_framework import status

pytestmark = pytest.mark.django_db


def test_can_get_posts_list(client):
    url = reverse('posts-list')
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


def test_can_get_users_list(client):
    url = reverse('users-list')
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


def test_login_post_only(client):
    url = reverse('users-login')
    get_response = client.get(url)
    assert get_response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


def test_user_can_login(client, django_user_model):
    email = "user1@example.com"
    password = "bar"
    django_user_model.objects.create_user(email=email, password=password)
    url = reverse('users-login')
    payload = {"email": email, "password": password}
    response = client.post(url, data=payload)
    assert response.status_code == status.HTTP_200_OK


def test_user_can_register(client, django_user_model):
    email = "user1@example.com"
    password = "bar"
    django_user_model.objects.create_user(email=email, password=password)
    url = reverse('users-register')
    payload = {"email": email, "password": password}
    response = client.post(url, data=payload)
    assert response.status_code == status.HTTP_200_OK
