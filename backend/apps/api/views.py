from uuid import uuid4

from django.conf import settings
from django.contrib.auth import authenticate, login
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.response import Response

from apps.api.models import User, Post
from apps.api.serializers import UserSerializer, UserWriteSerializer, PostSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    # permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return UserSerializer
        return UserWriteSerializer

    def perform_create(self, serializer):
        user = serializer.save()
        user.set_password(self.request.data.get('password'))
        user.save()

    def perform_update(self, serializer):
        user = serializer.save()
        if 'password' in self.request.data:
            user.set_password(self.request.data.get('password'))
            user.save()

    @action(detail=False, methods=['GET'])
    def profile(self, request):
        if request.user.is_authenticated:
            serializer = self.serializer_class(request.user)
            return Response(status=status.HTTP_200_OK, data=serializer.data)
        return Response(status=status.HTTP_401_UNAUTHORIZED)

    @action(detail=False, methods=['POST'])
    def login(self, request):
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        user = authenticate(username=email, password=password)

        if user:
            login(request, user)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)

    @action(detail=False, methods=['POST'])
    def register(self, request):
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        username = str(email).split('@')[0]
        # password = user.set_password(raw_password)

        if User.objects.filter(email__iexact=email).exists():
            return Response({'status': 210})

        # user creation
        user = User.objects.create(
            email=email,
            password=password,
            username=username,
            is_admin=False,
        )
        user.set_password(password)
        user.save()
        return Response(UserSerializer(user).data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['POST'])
    def password_reset(self, request):
        if User.objects.filter(email=request.data['email']).exists():
            user = User.objects.get(email=request.data['email'])
            params = {'user': user, 'DOMAIN': settings.DOMAIN}
            send_mail(
                subject='Password reset',
                message=render_to_string('mail/password_reset.txt', params),
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[request.data['email']],
            )
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @action(detail=False, methods=['POST'])
    def password_change(self, request):
        if User.objects.filter(token=request.data['token']).exists():
            user = User.objects.get(token=request.data['token'])
            user.set_password(request.data['password'])
            user.token = uuid4()
            user.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer

    # permission_classes = (ReadOnly or IsAuthenticated,)

    def get_queryset(self):
        """ Опционально фильтровать результаты по параметрам author и approved """
        queryset = Post.objects.all()

        # 'author' url parameter
        author_id = self.request.query_params.get('author', None)
        if author_id is not None:
            queryset = queryset.filter(author_id=author_id)

        # 'approved' url parameter
        is_approved = self.request.query_params.get('approved', None)
        if is_approved is not None:
            approved: bool = False
            if 'true' in str(is_approved).lower():
                approved = True
            queryset = queryset.filter(is_approved=approved)

        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            title = request.data.get('title', None)
            text = request.data.get('text', None)
            author = request.data.get('author', None)
            new_post = Post.objects.create(title=title, text=text, author_id=author)
            return Response(PostSerializer(new_post).data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_404_NOT_FOUND)

    @action(detail=False, methods=['PUT'])
    def approve(self, request):
        user = User.objects.get(id=request.user.id)
        if user.is_admin:
            post_id = request.data.get('id', None)
            if post_id is not None:
                post = Post.objects.get(id=post_id)
                post.is_approved = True
                post.save()
                return Response(PostSerializer(post).data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
