from rest_framework import serializers

from apps.api.models import User, Post


class UserSerializer(serializers.ModelSerializer):
    registered_at = serializers.DateTimeField(format='%H:%M %d.%m.%Y', read_only=True)
    username = serializers.SerializerMethodField(read_only=True)

    def get_username(self, obj):
        return obj.username

    class Meta:
        model = User
        fields = ['email', 'username', 'registered_at', 'id', 'is_admin']


class UserWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password', 'username']


class PostSerializer(serializers.ModelSerializer):
    is_approved = serializers.SerializerMethodField(read_only=True)
    author_username = serializers.ReadOnlyField(source='author.username')

    def get_is_approved(self, obj):
        state = 'на модерации'
        if obj.is_approved:
            state = 'опубликован'
        return state

    # def get_author_username(self, obj):
    #     print('!>>!>!>!!>', type(object))
    #     return 'lol'

    class Meta:
        model = Post
        fields = ['title', 'text', 'author_username', 'author', 'id', 'is_approved']
