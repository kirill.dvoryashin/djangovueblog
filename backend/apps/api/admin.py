from django.contrib import admin
from django.contrib.auth.models import Group

from apps.api.models import User, Post

admin.site.register(User)
admin.site.register(Post)
# Unregister the Group model from admin.
admin.site.unregister(Group)
