whynot
======

## Development
+ run `docker-compose up --build`
+ `docker-compose run --rm backend python manage.py makemigrations`
+ `docker-compose run --rm backend python manage.py migrate`
+ `docker-compose run --rm backend python manage.py createsuperuser`


## Tests
+ `docker-compose run --rm backend py.test`
